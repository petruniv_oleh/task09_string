package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.bigTask.BigTask;
import com.oleh.model.bigTask.Sentence;
import com.oleh.model.utils.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.util.*;

public class View implements ViewInt {

    private static Logger logger = LogManager.getLogger(View.class.getName());
    private Scanner scanner;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private Controller controller;


    private Locale locale;
    private ResourceBundle resourceBundle;

    public View() {
        controller = new Controller();
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        scanner = new Scanner(System.in);
        makeMenuMap();
        menuMapMethods = new LinkedHashMap<String, Printable>();
        menuMapMethods.put("1", this::testStringUtils);
        menuMapMethods.put("2", this::switchToUk);
        menuMapMethods.put("3", this::switchToEn);
        menuMapMethods.put("4", this::switchToJp);
        menuMapMethods.put("5", this::switchToEs);
        menuMapMethods.put("6", this::testSentence);
        menuMapMethods.put("7", this::splitString);
        menuMapMethods.put("8", this::replaceVowels);
        menuMapMethods.put("9", this::getBigTaskRes);

        showMenu();
    }

    public void makeMenuMap() {
        menuMap = new LinkedHashMap<>();
        menuMap.put("1", resourceBundle.getString("1"));
        menuMap.put("2", resourceBundle.getString("2"));
        menuMap.put("3", resourceBundle.getString("3"));
        menuMap.put("4", resourceBundle.getString("4"));
        menuMap.put("5", resourceBundle.getString("5"));
        menuMap.put("6", resourceBundle.getString("6"));
        menuMap.put("7", resourceBundle.getString("7"));
        menuMap.put("8", resourceBundle.getString("8"));
        menuMap.put("9", resourceBundle.getString("9"));


    }


    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\n"+resourceBundle.getString("0"));
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println(resourceBundle.getString("select"));
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void testStringUtils() {
        logger.info("test string utils");
        StringUtils stringUtils = controller.getStringUtils();
        System.out.println(stringUtils.doWork(123, " ", 23.4f, 2.9, true, "Hello", false));
    }

    @Override
    public void switchToEn() {
        logger.info("change locale to en");
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        makeMenuMap();
        showMenu();
    }

    @Override
    public void switchToUk() {
        logger.info("change locale to ua");
        locale = new Locale("ua");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        makeMenuMap();
        showMenu();
    }

    @Override
    public void switchToJp() {
        logger.info("change locale to jp");
        locale = new Locale("jp");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        makeMenuMap();
        showMenu();
    }

    @Override
    public void switchToEs() {
        logger.info("change locale to es");
        locale = new Locale("es");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        makeMenuMap();
        showMenu();
    }

    @Override
    public void testSentence() {
        logger.info("testing sentence");
        System.out.println(resourceBundle.getString("sentence"));
        String line = scanner.nextLine();
        boolean isRight = controller.testSentence(line);
        System.out.println(isRight);

    }

    @Override
    public void splitString() {
        logger.info("spliting string");
        System.out.println(resourceBundle.getString("sentence"));
        String line = scanner.nextLine();
        System.out.println(resourceBundle.getString("res"));
        String[] splitedString = controller.getSplitedString(line);
        for (int i = 0; i < splitedString.length; i++) {
            System.out.println(splitedString[i]);
        }

    }

    @Override
    public void replaceVowels() {
        logger.info("replacing vowels");
        System.out.println(resourceBundle.getString("sentence"));
        String line = scanner.nextLine();
        System.out.println(resourceBundle.getString("res"));
        System.out.println(controller.getSentenceWithREplacedVowels(line));
    }

    @Override
    public void getBigTaskRes() {
        logger.info("giving the result for big task");
        System.out.println("The solution of big task:");
        BigTask bigTask = new BigTask(getFileFromResources("book.txt"));
        String s = bigTask.maxAmountOfSentencesWithSameWord();
        System.out.println("1. Знайти найбільшу кількість речень тексту, в яких є однакові слова.");
        System.out.println(s);
        System.out.println("2. Вивести всі речення заданого тексту у порядку зростання кількості слів у\n" +
                "кожному з них.");
        List<Sentence> sentencesSortedByWords = bigTask.getSentencesSortedByWords();
        for (int i = 0; i < sentencesSortedByWords.size(); i++) {
            System.out.println(sentencesSortedByWords.get(i));
        }
        System.out.println("3. Знайти таке слово у першому реченні, якого немає ні в одному з інших\n" +
                "речень.");
        System.out.println(bigTask.getUniqWordOfFirstSentence());
        System.out.println("4. У всіх запитальних реченнях тексту знайти і надрукувати без повторів\n" +
                "слова заданої довжини.");
        System.out.println(bigTask.getWordOfQuestionSentences(2));
        System.out.println("5. У кожному реченні тексту поміняти місцями перше слово, що починається\n" +
                "на голосну букву з найдовшим словом.");
        System.out.println(bigTask.replaceAllVowelsToLongest());
        System.out.println("6. Надрукувати слова тексту в алфавітному порядку по першій букві. Слова,\n" +
                "що починаються з нової букви, друкувати з абзацного відступу.");
        String wordsAlphabetic = bigTask.getWordsAlphabetic();
        System.out.println(wordsAlphabetic);
        System.out.println("7. Відсортувати слова тексту за зростанням відсотку голосних букв\n" +
                "(співвідношення кількості голосних до загальної кількості букв у слові).");
        System.out.println(bigTask.sortWordsByVowels());
        System.out.println("8. Слова тексту, що починаються з голосних букв, відсортувати в\n" +
                "алфавітному порядку по першій приголосній букві слова.");
        System.out.println(bigTask.sortVowelsAlphabetic());
        System.out.println("9. Всі слова тексту відсортувати за зростанням кількості заданої букви у\n" +
                "слові. Слова з однаковою кількістю розмістити у алфавітному порядку.");
        System.out.println(bigTask.sortByLetter("a"));
        System.out.println("10. Є текст і список слів. Для кожного слова з заданого списку знайти, скільки\n" +
                "разів воно зустрічається у кожному реченні, і відсортувати слова за\n" +
                "спаданням загальної кількості входжень.");
        System.out.println(bigTask.sortWordsByPopularity());
        System.out.println("11. У кожному реченні тексту видалити підрядок максимальної довжини, що\n" +
                "починається і закінчується заданими символами.");
        System.out.println(bigTask.deleteByLetters("a", "n"));
        System.out.println("12. З тексту видалити всі слова заданої довжини, що починаються на\n" +
                "приголосну букву.");
        System.out.println(bigTask.deleteWordsStNoVowelByLength(4));
        System.out.println("13. Відсортувати слова у тексті за спаданням кількості входжень заданого\n" +
                "символу, а у випадку рівності – за алфавітом.");
        System.out.println(bigTask.sortByLetterDesc("t"));
        System.out.println("14. У заданому тексті знайти підрядок максимальної довжини, який є\n" +
                "паліндромом, тобто, читається зліва на право і справа на ліво однаково.");
        System.out.println(bigTask.getMaxPalindrome());
        System.out.println("15. Перетворити кожне слово у тексті, видаливши з нього всі наступні\n" +
                "(попередні) входження першої (останньої) букви цього слова.");
        System.out.println(bigTask.wordsWithoutRepeatingFirstAndLastLetter());
        System.out.println("16. У певному реченні тексту слова заданої довжини замінити вказаним\n" +
                "підрядком, довжина якого може не співпадати з довжиною слова.");
        System.out.println(bigTask.changeWordInSentence(4, "the text to replace"));

    }


    private File getFileFromResources(String fileName) {

        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }
}
