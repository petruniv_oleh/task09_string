package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void testStringUtils();
    void switchToEn();
    void switchToUk();
    void switchToJp();
    void switchToEs();
    void testSentence();
    void splitString();
    void replaceVowels();
    void getBigTaskRes();
}
