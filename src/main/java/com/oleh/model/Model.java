package com.oleh.model;

import com.oleh.model.utils.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Model implements ModelInt {


    public StringUtils makeStringUtils() {
        return new StringUtils();
    }

    @Override
    public boolean testSentence(String sentence) {
        Pattern pattern = Pattern.compile("(^[A-Z].*[.])");
        Matcher matcher = pattern.matcher(sentence);
        return matcher.find();
    }

    @Override
    public String[]  getSplitedString(String sentence) {
        return sentence.split("(the|you)");
    }

    @Override
    public String replaceVowels(String sentence) {
        return sentence.replaceAll("([AEIOUY]|[aeiouy])", "_");
    }
}
