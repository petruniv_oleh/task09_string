package com.oleh.model;

import com.oleh.model.utils.StringUtils;

public interface ModelInt {
    StringUtils makeStringUtils();
    boolean testSentence(String sentence);
    String[]  getSplitedString(String sentence);
    String replaceVowels(String sentence);

}
