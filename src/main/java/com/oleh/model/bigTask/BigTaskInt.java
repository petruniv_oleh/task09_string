package com.oleh.model.bigTask;

import java.io.File;
import java.util.List;

public interface BigTaskInt {

    void readFile(File file);
    String maxAmountOfSentencesWithSameWord();
    List<Sentence> getSentencesSortedByWords();
    String getUniqWordOfFirstSentence();
    String getWordOfQuestionSentences(int length);
    List<Sentence> replaceAllVowelsToLongest();
    String getWordsAlphabetic();
    String sortWordsByVowels();
    String sortVowelsAlphabetic();
    String sortByLetter(String letter);
    String sortWordsByPopularity();
    String sortByLetterDesc(String letter);
    String deleteByLetters(String begin, String end);
    String deleteWordsStNoVowelByLength(int length);
    String changeWordInSentence(int length, String replacement);
    String getMaxPalindrome();
    String wordsWithoutRepeatingFirstAndLastLetter();

}
