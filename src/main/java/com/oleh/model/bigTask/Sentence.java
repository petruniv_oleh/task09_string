package com.oleh.model.bigTask;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Sentence {
    private String sentence;
    private int amountOfWords;


    public Sentence(String sentence) {
        this.sentence = sentence;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public int getAmountOfWords() {
        return amountOfWords;
    }

    public String getWithoutSub(String start, String end){
        Pattern pattern = Pattern.compile(start+".*"+end);
        Matcher matcher = pattern.matcher(sentence);
        if (matcher.find()){
            String group = matcher.group();
            String s = sentence.substring(0, matcher.start());
            s+=sentence.substring(matcher.end());
            return s;
        }
        return sentence;
    }

    public void setAmountOfWords(int amountOfWords) {
        this.amountOfWords = amountOfWords;
    }

    @Override
    public String toString() {
        return  sentence;
    }
}
