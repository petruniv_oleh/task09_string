package com.oleh.model.bigTask;


import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BigTask implements BigTaskInt {
    private String book;
    private List<Word> words = new ArrayList<>();
    private List<Sentence> sentences = new ArrayList<>();

    public BigTask(File file) {
        readFile(file);
        preWork();
        makeSentencesSet();
        countWordsInSentences();
    }

    private void preWork() {
        book.replaceAll("((.*?)\\t|[ ]{2})", " ");
        makeWordSet();
    }

    private void makeWordSet() {
        Pattern pattern = Pattern.compile("[A-Za-z]+");
        Matcher matcher = pattern.matcher(book);
        Set<String> tempWords = new HashSet<>();
        while (matcher.find()) {
            tempWords.add(matcher.group().toLowerCase());
        }

        Iterator<String> iterator = tempWords.iterator();
        while (iterator.hasNext()) {
            words.add(new Word(iterator.next()));
        }
    }

    private void countWordsInSentences() {
        Pattern pattern = Pattern.compile("[\\w-]+");
        for (int i = 0; i < sentences.size(); i++) {
            Matcher matcher = pattern.matcher(sentences.get(i).getSentence());
            int counter = 0;
            while (matcher.find()) {
                counter++;
            }
            sentences.get(i).setAmountOfWords(counter);
        }


    }

    private List<String> getAllUniqueWords() {
        Pattern pattern = Pattern.compile("([A-Za-z]\\w+)(?![\\s\\S]*\\1)");
        Matcher matcher = pattern.matcher(book);
        List<String> uniques = new ArrayList<>();
        while (matcher.find()) {
            uniques.add(matcher.group());
        }
        return uniques;
    }

    private void makeSentencesSet() {
        Pattern pattern = Pattern.compile("[^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[.!?]?['\"]?(?=\\s|$)");
        Matcher matcher = pattern.matcher(book);
        while (matcher.find()) {
            sentences.add(new Sentence(matcher.group()));
        }
    }

    @Override
    public void readFile(File file) {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            BufferedReader bf = new BufferedReader(new FileReader(file));
            String str = null;
            while ((str = bf.readLine()) != null) {
                stringBuilder.append(str);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        book = stringBuilder.toString();
    }

    /**
     * 1-st from big task
     *
     * @return
     */
    @Override
    public String maxAmountOfSentencesWithSameWord() {
        int[] sentencesArr = new int[words.size()];
        int k = 0;
        Iterator<Word> iterator = words.iterator();
        while (iterator.hasNext()) {
            Pattern pattern = Pattern.compile(iterator.next().getWord());
            for (int i = 0; i < sentences.size(); i++) {
                Matcher matcher = pattern.matcher(sentences.get(i).getSentence());
                if (matcher.find()) {
                    sentencesArr[k]++;
                    matcher.group();
                }
            }
            k++;
        }
        int max = sentencesArr[0];
        for (
                int i = 0;
                i < sentencesArr.length; i++) {
            if (max < sentencesArr[i]) {
                max = sentencesArr[i];
            }
        }
        return "There is " + max + " sentences with the same word!";
    }


    /**
     * 2-nd from big task
     *
     * @return
     */
    @Override
    public List<Sentence> getSentencesSortedByWords() {
        sentences.sort(((o1, o2) -> o1.getAmountOfWords() - o2.getAmountOfWords()));
        return sentences;
    }


    /**
     * 3-d from big task
     *
     * @return
     */
    @Override
    public String getUniqWordOfFirstSentence() {
        List<String> allUniqueWords = getAllUniqueWords();
        for (int i = 0; i < allUniqueWords.size(); i++) {
            if (sentences.get(0).getSentence().contains(allUniqueWords.get(i))) {
                return allUniqueWords.get(i);
            }
        }

        return "There is no unique words in first sentence";
    }

    private String getAllQuestionSentences() {
        Pattern pattern = Pattern.compile("^[A-Z][^.!?\\s][^.!?]*(?:[.!?](?!['\"]?\\s|$)[^.!?]*)*[?]");
        Matcher matcher = pattern.matcher(book);
        StringBuilder questionSen = new StringBuilder();
        while (matcher.find()) {
            questionSen.append(matcher.group());
        }
        return questionSen.toString();
    }

    /**
     * 4-st from big task
     *
     * @param length
     * @return
     */
    @Override
    public String getWordOfQuestionSentences(int length) {
        String allQuestionSentences = getAllQuestionSentences();
        StringBuilder sb = new StringBuilder();
        Pattern pattern = Pattern.compile("([a-z]\\w{" + length + ",})(?![\\s\\S]*\\1)");

        Matcher matcher = pattern.matcher(allQuestionSentences);
        while (matcher.find()) {
            sb.append(matcher.group() + " ");
        }
        return sb.toString();
    }


    private String getVowel(String sentence) {
        Pattern pattern = Pattern.compile("[AYIOEayioe]\\w+[^A-Za-z\\w+]");
        Matcher matcher = pattern.matcher(sentence);
        if (matcher.find()) {
            return matcher.group();
        }
        return "";
    }

    private String getLongest(String sentence) {
        String[] split = sentence.split("[ ,:]");
        Arrays.sort(split);
        return split[split.length - 1];
    }

    /**
     * 5-st from big task
     *
     * @return
     */
    @Override
    public List<Sentence> replaceAllVowelsToLongest() {
        for (int i = 0; i < sentences.size(); i++) {
            String longest = getLongest(sentences.get(i).getSentence());
            String vowel = getVowel(sentences.get(i).getSentence());
            sentences.get(i).getSentence().replaceAll(longest, vowel);
        }
        return sentences;
    }

    /**
     * 6-st from big task
     *
     * @return
     */
    @Override
    public String getWordsAlphabetic() {
        String[] alphabeth = {"Aa", "Bb", "Cc", "Dd", "Ee", "Ff", "Gg",
                "Hh", "Ii", "Jj", "Kk", "Ll", "Mm", "Nn", "Oo", "Pp", "Qq", "Rr",
                "Ss", "Tt", "Uu", "Vv", "Ww", "Xx", "Yy", "Zz"};
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < alphabeth.length; i++) {
            Pattern pattern = Pattern.compile("[ " + alphabeth[i] + "]\\w+");
            for (int j = 0; j < words.size(); j++) {
                Matcher matcher = pattern.matcher(words.get(j).getWord());
                if (matcher.find()) {
                    stringBuilder.append(matcher.group() + " ");
                }
            }
            stringBuilder.append("\n");
        }
        return stringBuilder.toString();
    }


    /**
     * 7-st from big task
     *
     * @return
     */
    @Override
    public String sortWordsByVowels() {
        StringBuilder stringBuilder = new StringBuilder();
        Collections.sort(words);
        for (int i = 0; i < words.size(); i++) {
            stringBuilder.append(words.get(i).getWord() + " ");
        }

        return stringBuilder.toString();
    }

    private boolean isStartsWithVowel(String word) {
        Pattern pattern = Pattern.compile("(?<!\\w)[EAYOIUeayoiu]\\w+");
        Matcher matcher = pattern.matcher(word);
        if (matcher.find()) {
            return true;
        }
        return false;

    }

    private Character getFirstNonVowel(String word) {
        Pattern pattern = Pattern.compile("(?<!\\w)[EAYOIeayoiu]+[^EAYOIeayoiu ]");
        Matcher matcher = pattern.matcher(word);
        if (matcher.find()) {
            String group = matcher.group();
            return group.toLowerCase().charAt(group.length()-1);
        }
        return null;
    }

    /**
     * 8-st from big
     *
     * @return
     */
    @Override
    public String sortVowelsAlphabetic() {
        List<String> vowelsWords = new ArrayList<>();
        for (int i = 0; i < words.size(); i++) {
            if (isStartsWithVowel(words.get(i).getWord())) {
                vowelsWords.add(words.get(i).getWord());
            }
        }
        Collections.sort(vowelsWords, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Character firstNonVowel1 = getFirstNonVowel(o1);
                Character firstNonVowel2 = getFirstNonVowel(o2);
                if (firstNonVowel1 == null && firstNonVowel2 == null) {
                    return 0;
                }
                if (firstNonVowel1 == null){
                    return -1;
                }
                if (firstNonVowel2 == null){
                    return 1;
                }
                return firstNonVowel1.compareTo(firstNonVowel2);
            }
        });



        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < vowelsWords.size(); i++) {
            stringBuilder.append(vowelsWords.get(i) + " ");
        }

        return stringBuilder.toString();
    }

    /**
     * 9-st from big task
     *
     * @param letter
     * @return
     */
    @Override
    public String sortByLetter(String letter) {

        words.sort(new Comparator<Word>() {
            @Override
            public int compare(Word o1, Word o2) {
                if (o1.countLetter(letter) - o2.countLetter(letter) == 0) return o1.getWord().compareTo(o2.getWord());
                return o1.countLetter(letter) - o2.countLetter(letter);
            }
        });



        return wordsListToString(words);
    }

    private void countWordsPopularity(){
        for (int i = 0; i < words.size(); i++) {
            words.get(i).countPopularity(book);
        }
    }

    private String wordsListToString(List<Word> wordsList){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < wordsList.size(); i++) {
            stringBuilder.append(wordsList.get(i).getWord() + " ");
        }
        return stringBuilder.toString();
    }
    /**
     * 10-st from big task
     * @return
     */
    @Override
    public String sortWordsByPopularity() {
        countWordsPopularity();
        words.sort(new Comparator<Word>() {
            @Override
            public int compare(Word o1, Word o2) {
                return o2.getPopularity()-o1.getPopularity();
            }
        });
        return wordsListToString(words);
    }



    /**
     * 11 - st from big task
     * @param begin
     * @param end
     * @return
     */
    @Override
    public String deleteByLetters(String begin, String end) {
        List<String>  newSentences = new ArrayList<>();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < sentences.size(); i++) {
            newSentences.add(sentences.get(i).getWithoutSub(begin, end));
        }
        for (int i = 0; i < newSentences.size(); i++) {
            stringBuilder.append(newSentences.get(i)+"\n");
        }
        return stringBuilder.toString();
    }

    /**
     *12- th from big task
     * @param length
     * @return
     */
    @Override
    public String deleteWordsStNoVowelByLength(int length) {
        Pattern pattern = Pattern.compile("(?<!\\w)[^EAYOIeayoiu\\s]\\S{"+(length-1)+"} ");
        Matcher matcher = pattern.matcher(book);
        String s = book.replaceAll("(?<!\\w)[^EAYOIeayoiu\\s]\\w+", "");
        return s;
    }

    /**
     * 13 - th from big task
     * @return
     */
    @Override
    public String sortByLetterDesc(String letter) {
        words.sort(new Comparator<Word>() {
            @Override
            public int compare(Word o1, Word o2) {
                if (o1.countLetter(letter) - o2.countLetter(letter) == 0) return o2.getWord().compareTo(o1.getWord());
                return o2.countLetter(letter) - o1.countLetter(letter);
            }
        });
        return wordsListToString(words);
    }




    /**
     * 14 - th from big task
     * @return
     */
    @Override
    public String getMaxPalindrome() {
        //imposible with just regex,
        // without regex it will take to much computing

        return "imposible";
    }


    @Override
    public String wordsWithoutRepeatingFirstAndLastLetter() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < words.size(); i++) {
            String word = words.get(i).getWord();
            String substring = word.substring(1, word.length() - 2).replaceAll(""+word.charAt(0),"")
                    .replaceAll(""+word.charAt(word.length()-1), "");
            stringBuilder.append(word.charAt(0));
            stringBuilder.append(substring);
            stringBuilder.append(word.charAt(word.length()-1)+" ");

        }
        return stringBuilder.toString();
    }

    /**
     * 16-th from big task
     * @param length
     * @return
     */
    @Override
    public String changeWordInSentence(int length, String replacement) {
        Random random = new Random();
        int sentenceNumber = random.nextInt(sentences.size()-1);
        Pattern pattern = Pattern.compile("(?<!\\w)[^\\s]\\S{"+(length-1)+"}[^.,!?]");
        String s = sentences.get(sentenceNumber).getSentence().replaceAll("(?<!\\w)[^\\s]\\S{" + (length - 1) + "}[^.,!?]", replacement);
        return s;
    }
}
