package com.oleh.model.bigTask;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Word implements Comparable<Word>{
    private String word;
    private Double vowelPer;
    private Integer popularity;

    public Word(String word) {
        this.word = word;
        countVowelPer();
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    @Override
    public int compareTo(Word o) {
        return vowelPer.compareTo(o.vowelPer);
    }

    private void countVowelPer(){
        Pattern pattern = Pattern.compile("[AYOIUEayoiue]");
        Matcher matcher = pattern.matcher(word);
        int counter = 0;
        while (matcher.find()){
            counter++;
        }
        vowelPer = (double)counter/(double) word.length();
    }


    public int countLetter(String  letter){
        Pattern pattern = Pattern.compile("["+letter.toLowerCase()+letter.toUpperCase()+"]");
        Matcher matcher = pattern.matcher(word);
        int counter = 0;
        while (matcher.find()){
            counter++;
        }
        return counter;
    }
    public double getVowelPer() {
        return vowelPer;
    }

    public void setVowelPer(double vowelPer) {
        this.vowelPer = vowelPer;
    }

    public void countPopularity(String text){
        Pattern pattern = Pattern.compile(word.toLowerCase());
        Matcher matcher = pattern.matcher(text.toLowerCase());
        int counter=0;
        while (matcher.find()){
            counter++;
        }
        this.popularity = counter;
    }

    public Integer getPopularity() {
        return popularity;
    }

    public void setPopularity(Integer popularity) {
        this.popularity = popularity;
    }

    @Override
    public String toString() {
        return word;
    }
}
