package com.oleh.controller;

import com.oleh.model.utils.StringUtils;

public interface ControllerInt {
    StringUtils getStringUtils();
    boolean testSentence(String sentence);
    String[]  getSplitedString(String sentence);
    String getSentenceWithREplacedVowels(String sentence);
}
