package com.oleh.controller;

import com.oleh.model.Model;
import com.oleh.model.utils.StringUtils;

public class Controller implements ControllerInt{
    Model model;

    public Controller() {
        model = new Model();
    }

    public StringUtils getStringUtils() {
        return model.makeStringUtils();
    }

    @Override
    public boolean testSentence(String sentence) {
        return model.testSentence(sentence);
    }

    @Override
    public String[] getSplitedString(String sentence) {
        return model.getSplitedString(sentence);
    }

    @Override
    public String getSentenceWithREplacedVowels(String sentence) {
        return model.replaceVowels(sentence);
    }
}
